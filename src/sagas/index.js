import {all, fork, call} from 'redux-saga/effects';
import {watchDecreaseCounter} from '../screens/Home/sagas';
export default function* rootSaga() {
  yield all([call(watchDecreaseCounter)]);
}
