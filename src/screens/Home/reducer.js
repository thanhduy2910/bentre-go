import {GET_TEXT, SET_TEXT, ERROR_TEXT} from './constant';
// Initial State
const initialState = {
  counter: 0,
  isLoading: false,
};

// Redux: Counter Reducer
const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'DECREASE_COUNTER_START': {
      return {
        isLoading: true,
      };
    }
    case 'DECREASE_COUNTER_SUCCESS': {
      return {
        ...state,
        counter: action.value,
      };
    }

    default: {
      return state;
    }
  }
};

// Exports
export default counterReducer;
