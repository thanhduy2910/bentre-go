// Imports: Dependencies
import {delay, takeLatest, put} from 'redux-saga/effects';
function* decreaseCounter(action) {
  try {
    yield put({
      type: 'DECREASE_COUNTER_SUCCESS',
      value: 2,
    });
  } catch (error) {}
}

// Generator: Watch decrease Counter
export function* watchDecreaseCounter() {
  yield takeLatest('DECREASE_COUNTER_START', decreaseCounter);
}
