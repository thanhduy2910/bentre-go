export const decreaseCounterStart = () => {
  return {
    type: 'DECREASE_COUNTER_START',
  };
};
export const decreaseCounter = (value) => {
  return {
    type: 'DECREASE_COUNTER_SUCCESS',
    payload: value,
  };
};
